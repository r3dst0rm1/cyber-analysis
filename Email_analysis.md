# Emails Field
<details><summary>+</summary>

**From**	
The sender's address.

**To**	
The receiver's address, including CC and BCC.

**Date**	
Timestamp, when the email was sent.

**Subject**	
The subject of the email.

**Return Path**	
The return address of the reply, a.k.a. "Reply-To".
If you reply to an email, the reply will go to the address mentioned in this field.

**Domain Key and DKIM Signatures**	
Email signatures are provided by email services to identify and authenticate emails.

**SPF**	
Shows the server that was used to send the email.
It will help to understand if the actual server is used to send the email from a specific domain.

**Message-ID**	
Unique ID of the email.

**MIME-Version**	
Used MIME version.
It will help to understand the delivered "non-text" contents and attachments.

**X-Headers**	
The receiver mail providers usually add these fields.
Provided info is usually experimental and can be different according to the mail provider.

**X-Received**	
Mail servers that the email went through.

**X-Spam Status**	
Spam score of the email.

**X-Mailer**	
Email client name.

</details>

# emlAnalyzer 
<details><summary>+</summary>

`-i` 	File to analyse

`-i`  /path-to-file/filename

Note: Remember, you can either give a full file path or navigate to the required folder using the "cd" command.

`--header`	Show header

`-u`	Show URLs

`--text`	Show cleartext data

`--extract-all`	Extract all attachments

</details>

# Check mail reputation 

<details><summary>+</summary>

--> [ emailrep](https://emailrep.io/)

Here, if you find any suspicious URLs and IP addresses, consider using some OSINT tools for further investigation. While we will focus on using Virustotal and InQuest, having similar and alternative services in the analyst toolbox is worthwhile and advantageous.

**VirusTotal**
A service that provides a cloud-based detection toolset and sandbox environment.

**InQuest**
A service provides network and file analysis by using threat analytics.

**IPinfo.io**
A service that provides detailed information about an IP address by focusing on geolocation data and service provider.

**Talos Reputation**
An IP reputation check service is provided by Cisco Talos.

**Urlscan.io**
A service that analyses websites by simulating regular user behaviour.

**Browserling**
A browser sandbox is used to test suspicious/malicious links.

**Wannabrowser**
A browser sandbox is used to test suspicious/malicious links.

</details>

