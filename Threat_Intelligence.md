# scanning and analysing websites
<details><summary>+</summary>
--> [urlscan](https://urlscan.io/)
</details>

# Talos Intelligence &  Abuse project
<details><summary>+</summary>

Abuse.ch is a research project hosted by the Institue for Cybersecurity and Engineering at the Bern University of Applied Sciences in Switzerland. It was developed to identify and track malware and botnets through several operational platforms developed under the project 

-->  [abuse](https://abuse.ch/)


Cisco assembled a large team of security practitioners called Cisco Talos to provide actionable intelligence, visibility on indicators, and protection against emerging threats through data collected from their products. The solution is accessible as Talos Intelligence.

--> [talosintelligence ](https://talosintelligence.com/)

</details>
